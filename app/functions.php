<?php
function currencyConverter($fromCurrency,$toCurrency,$amount) {	
	$fromCurrency = urlencode($fromCurrency);
	$toCurrency = urlencode($toCurrency);	
	$encode_amount = 1;
	$url = "https://api.exchangeratesapi.io/latest?base=". $fromCurrency ."&symbols=". $toCurrency;

	$get = file_get_contents($url);
	$array = json_decode($get, true);
	$exhangeRate = (float)array_values($array['rates'])[0];

	$convertedAmount = $amount*$exhangeRate;
	$data = array( 'exhangeRate' => $exhangeRate, 'convertedAmount' =>$convertedAmount, 'fromCurrency' => strtoupper($fromCurrency), 'toCurrency' => strtoupper($toCurrency));
	echo json_encode( $data );	
}
?> 

